# Get-Set Prop

A function used to get or set value on a deep nested object using dot notation.

## Installation

```
npm install @letumfalx/get-set-prop
```

## Get Function

The get function split the key, then traverse through the given object to get the target value. This will check if the prop key is owned by object instead of checking if the value is `undefined`. If the prop key is not found, this function will return the default value given.

### Usage

```
getProp(object, key, defaultValue = null, options = {});
```

#### Basic Usage

```
var getProp = require('@letumfalx/get-set-prop').getProp;

var referenceObject = {
  key1: {
    nestedOne: 1,
    nested: [
      'array_value_1'
    ]
  },
  key2: null,
  key3: undefined
};

getProp(referenceObject, 'key1.nestedOne', null); // returns 1
getProp(referenceObject, 'key1.nested.0', null); // returns 'array_value_1'
getProp(referenceObject, 'key1.nested', null); // returns [ 'array_value_1' ]
getProp(referenceObject, 'key2', 'not null'); // returns null
getProp(referenceObject, 'key3', 'not undefined'); // returns undefined
getProp(referenceObject, 'key4', null); // returns null
getProp(referenceObject, 'key1.nested.1', null); // returns null
```

#### Options

There are additional options you can pass as the fourth/last parameter:

```
{
  separator: String = '.',
  enumerableOnly: Boolean = true
}
```

##### separator

This is the string to use for splitting the key. If you are using the default `.` on any of your key, you set other character that is most likely not used as character for the key of your object like `|`.

```
var getProp = require('@letumfalx/get-set-prop').getProp;

var referenceObject = {
  'a.1': {
    'b.1': 2
  }
};

getProp(referenceObject, 'a.1|b.1', null, { separator: '|' }); // returns 2
getProp(referenceObject, 'a.1|b.2', null, { separator: '|' }); // returns null
```

##### enumerableOnly

Set this to true if you want to search only for enumerable values (visible in `foreach`), otherwise set this to false. This defaults to `true`. This is usually used for getting the `length` of an `array` as the `length` is not enumerable but is owned by the array.

```
var getProp = require('../../src').getProp;

var referenceObject = {
  a: [
    1,
    2
  ],
  b: 5
};

Object.defineProperty(referenceObject, 'b', { enumerable: false });

getProp(referenceObject, 'a.length', null, { enumerableOnly: false }); // returns 2
getProp(referenceObject, 'a.length', null, { enumerableOnly: true }); // returns null
getProp(referenceObject, 'b', null, { enumerableOnly: false }); // returns 5
getProp(referenceObject, 'b', null, { enumerableOnly: true }); // returns null
```

## Set Function

The set function split the key, then traverse through the given value to the target property key. If it cannot go deeper due to encountering non-object value, it will throw a `TypeError`. If ever it encounters a `undefined`, `null`, or non-existing key, it will set its value to an empty object ( `{}` ) so we can continue to traverse the object up to the last property key. If traversing is successful, will set the last property key to the value given.

### Usage

```
setProp(object, key, value, options = {});
```

```
var setProp = require('../../src').setProp;

var referenceObject = {
  nested: {
    key1: 999,
    key2: null,
    key3: undefined,
    key4: [
      {
        arr1: 888
      }
    ],
    key5: {
      value: {
        valueInside: 777
      }
    }
  }
};

setProp(referenceObject, 'nested.key1', 1); // mod1
setProp(referenceObject, 'nested.key2.value2', 2); // mod2
setProp(referenceObject, 'nested.key3.value.3', 3); // mod3
setProp(referenceObject, 'nested.key4.0.arr1', 'arr1'); // mod4
setProp(referenceObject, 'nested.key5', null); // mod5
setProp(referenceObject, 'nested.key4.999', {
  a: {
    b: 2
  }
}); // mod6
setProp(referenceObject, 'nested.not_exists.value.1', 'not_exists'); // mod7

console.log(referenceObject);
```

Above usage will output:

```
{
  nested: {
    key1: 1, // change by mod1
    key2: { value2: 2 }, // change by mod2
    key3: { value: { '3': 3 } }, // changed by mod3
    key4: [ { arr1: 'arr1' }, <998 empty items>, { a: { b: 2 } } ], // changed by mod4
    key5: null, // changed by mod5
    not_exists: { value: { '1': 'not_exists' } } // change by mod6
  }
}
```

#### Options

There are additional options you can pass as the fourth/last parameter:

```
{
  separator: String = '.'
}
```

##### separator

This is the string to use for splitting the key. If you are using the default `.` on any of your key, you set other character that is most likely not used as character for the key of your object like `|`.

```
var setProp = require('@letumfalx/get-set-prop').setProp;

var referenceObject = {
  'nested.1': {
    'nested.2': {
      'nested.3': 1
    }
  }
};

setProp(referenceObject, 'nested.1|nested.2|nested.3', 2, { separator: '|' });

console.log(referenceObject);
```
