var setProp = require('../../src').setProp;

var referenceObject = {
  'nested.1': {
    'nested.2': {
      'nested.3': 1
    }
  }
};

setProp(referenceObject, 'nested.1|nested.2|nested.3', 2, { separator: '|' });

console.log(referenceObject);
