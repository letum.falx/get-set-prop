var setProp = require('../../src').setProp;

var referenceObject = {
  nested: {
    key1: 999,
    key2: null,
    key3: undefined,
    key4: [
      {
        arr1: 888
      }
    ],
    key5: {
      value: {
        valueInside: 777
      }
    }
  }
};

setProp(referenceObject, 'nested.key1', 1); //
setProp(referenceObject, 'nested.key2.value2', 2);
setProp(referenceObject, 'nested.key3.value.3', 3);
setProp(referenceObject, 'nested.key4.0.arr1', 'arr1');
setProp(referenceObject, 'nested.key5', null);
setProp(referenceObject, 'nested.key4.999', {
  a: {
    b: 2
  }
});
setProp(referenceObject, 'nested.not_exists.value.1', 'not_exists');

console.log(referenceObject);
