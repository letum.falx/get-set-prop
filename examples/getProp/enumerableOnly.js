var getProp = require('../../src').getProp;

var referenceObject = {
  a: [
    1,
    2
  ],
  b: 5
};

Object.defineProperty(referenceObject, 'b', { enumerable: false });

console.log({
  'a.length(enumerableOnly = false)': getProp(referenceObject, 'a.length', null, { enumerableOnly: false }), // returns 2
  'a.length(enumerableOnly = true)': getProp(referenceObject, 'a.length', null, { enumerableOnly: true }), // returns null
  'b(enumerableOnly = false)': getProp(referenceObject, 'b', null, { enumerableOnly: false }), // returns 5
  'b(enumerableOnly = true)': getProp(referenceObject, 'b', null, { enumerableOnly: true }) // returns null
});
