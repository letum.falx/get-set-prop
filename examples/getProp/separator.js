var getProp = require('../../src').getProp;

var referenceObject = {
  'a.1': {
    'b.1': 2
  }
};

console.log({
  'a.1|b.1': getProp(referenceObject, 'a.1|b.1', null, { separator: '|' }), // returns 2
  'a.1|b.2': getProp(referenceObject, 'a.1|b.2', null, { separator: '|' }), // returns null
});
