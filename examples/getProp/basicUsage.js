var getProp = require('../src').getProp;

var referenceObject = {
  key1: {
    nestedOne: 1,
    nested: [
      'array_value_1'
    ]
  },
  key2: null,
  key3: undefined
};

console.log({
  'key1.nestedOne': getProp(referenceObject, 'key1.nestedOne', null), // returns 1
  'key1.nested.0': getProp(referenceObject, 'key1.nested.0', null), // returns 'array_value_1'
  'key1.nested': getProp(referenceObject, 'key1.nested', null), // returns ['array_value_1']
  'key2': getProp(referenceObject, 'key2', 'not null'), // returns null
  'key3': getProp(referenceObject, 'key3', 'not undefined'), // returns undefined
  'key4': getProp(referenceObject, 'key4', null), // returns null
  'key5': getProp(referenceObject, 'key1.nested.1', null) // returns null
});
