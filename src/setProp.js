/**
 *
 * @param {Object} object the object where we set the value
 * @param {String} key the dot notation of the property to set
 * @param {any} value the value to set
 * @param {{ separator: String = '.' }} options additional options for setting the property
 * @return {Object} the object modified
 */
module.exports = (object, key, value, options = {}) => {
  if (typeof object !== 'object') {
    throw new TypeError(`given object should be type object, ${typeof object} given`);
  }

  object = object || {};

  const { separator = '.' } = options || {};
  const keys = key.split(typeof separator === 'undefined' ? defaultOptions.separator : separator);
  const length = keys.length;

  let obj = object;

  for (let i = 0; i < length - 1; ++i) {
    const keyIndex = keys[i];
    switch (typeof obj[keyIndex]) {
      case 'undefined':
        obj[keyIndex] = {};
        break;
      case 'object':
        if (!obj[keyIndex]) {
          obj[keyIndex] = {};
        }
        break;
      default:
        throw new TypeError('failed to follow object path as it contains non-object property');
    }

    obj = obj[keyIndex];
  }

  obj[keys[length - 1]] = value;

  return object;
};
