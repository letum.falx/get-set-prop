/**
 * Get the value of the property of a deep nested object using dot notation.
 *
 * @param {Object} object the object where we will get the property
 * @param {String} key the dot notation of the property to get
 * @param {any} [defaultValue=null] the value to return incase the property is not found
 * @param {{ separator: String = '.', enumerableOnly: Boolean = true }} options additional options for getting the property
 *
 * @return {any}
 */
module.exports = (object, key, defaultValue = null, options = {}) => {
  const {
    separator = '.',
    enumerableOnly = true
  } = options || {};

  const keys = key.split(separator);

  const length = keys.length;

  for (let i = 0; i < length; ++i) {
    const keyIndex = keys[i];

    const hasProperty = enumerableOnly
      ? Object.prototype.propertyIsEnumerable.call(object, keyIndex)
      : Object.prototype.hasOwnProperty.call(object, keyIndex);

    if (!hasProperty) {
      return defaultValue;
    }

    object = object[keyIndex];
  }

  return object;
};
